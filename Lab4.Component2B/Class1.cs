﻿using ComponentFramework;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Component2B
{
    public class Układ_Fonii2 : AbstractComponent
    {
        private IWzmacniacz wzmacniacz = new Wzmacniacz();

        public override string ToString()
        {
            return "Wzmacnaicz 2";
        }

        public static IWzmacniacz GetWzmacniacz(object component)
        {
            return (component as Układ_Fonii2).wzmacniacz;
        }

        public Układ_Fonii2()
        {
            RegisterProvidedInterface(typeof(IWzmacniacz), wzmacniacz);
        }

        public override void InjectInterface(Type type, object impl)
        {
            RegisterProvidedInterface(type, impl);
        }
    }
}
