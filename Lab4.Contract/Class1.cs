﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Contract
{
    public interface IFonia
    {
        void getSound();
        void volumeUp(double s);
        void volumeDown(double s);
        void wzmacniacz(IWzmacniacz I);
    }
}
