﻿using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Main
{
    public class Program
    {
        public static object CreateAndConfigureContainer()
        {
            Układ_Wejściowy Układ_Wejściowy = new Układ_Wejściowy();
            Układ_Fonii Układ_Fonii = new Układ_Fonii();
            Container Container = new Container();
            Container.RegisterComponent(Układ_Wejściowy);
            Container.RegisterComponent(Układ_Fonii);

            // return instance of properly configured container
            return Container;
        }

        public static void Main(string[] args)
        {
            var container = CreateAndConfigureContainer();
            (container as Container).GetInterface<IFonia>().getSound();
            (container as Container).GetInterface<IWzmacniacz>().wzmacniajDzwiek();
        }
    }
}
