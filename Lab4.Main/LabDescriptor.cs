﻿using System;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;
using ComponentFramework;
using Lab4.Component2B;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);
        public delegate object ResolveInstance(object container, Type type);

        #endregion

        #region P1

        public static Type Component1 = typeof(Układ_Wejściowy);
        public static Type Component2 = typeof(Układ_Fonii);

        public static Type ProvidedInterface = typeof(IFonia);
        public static Type RequiredInterface = typeof(IWzmacniacz);

        public static GetInstance GetInstanceOfProvidedInterface = (component1) => Układ_Wejściowy.GetFonia(component1);
        public static GetInstance GetInstanceOfRequiredInterface = (component2) => { if (component2 is Układ_Fonii) return Układ_Fonii.GetWzmacniacz(component2); else return Układ_Fonii2.GetWzmacniacz(component2); };

        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { (container as Container).RegisterComponent(component as AbstractComponent); };

        public static AreDependenciesResolved ResolvedDependencied = (container) => (container as Container).DependenciesResolved;

        public static ResolveInstance ResolveInterface = (container, type) => (container as Container).GetInterface(type);

        #endregion

        #region P3

        public static Type Component2B = typeof(Układ_Fonii2);

        #endregion
    }
}
