﻿using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2B;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.MainB
{
    public class Program
    {
        public static object CreateAndConfigureContainer()
        {
            Układ_Wejściowy Układ_Wejściowy = new Układ_Wejściowy();
            Układ_Fonii2 Układ_Fonii2 = new Układ_Fonii2();
            Container Container = new Container();
            Container.RegisterComponent(Układ_Wejściowy);
            Container.RegisterComponent(Układ_Fonii2);

            // return instance of properly configured container
            return Container;
        }

        public static void Main(string[] args)
        {
            var container = CreateAndConfigureContainer();

            (container as Container).GetInterface<IFonia>().getSound();
            (container as Container).GetInterface<IWzmacniacz>().wzmacniajDzwiek();
        }
    }
}
