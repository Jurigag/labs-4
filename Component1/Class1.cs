﻿using ComponentFramework;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Component1
{
    public class Układ_Wejściowy : AbstractComponent
    {
        private IWzmacniacz wzmacniacz;
        private Fonia fonia = new Fonia();

        public static IFonia GetFonia(object component)
        {
            return (component as Układ_Wejściowy).fonia;
        }

        public Układ_Wejściowy()
        {
            RegisterRequiredInterface(typeof(IWzmacniacz));
            RegisterProvidedInterface(typeof(IFonia), fonia);
        }

        public override void InjectInterface(Type type, object impl)
        {
            RegisterProvidedInterface(type, impl);
        }
    }
}
