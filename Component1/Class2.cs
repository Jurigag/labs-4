﻿using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Component1
{
    class Fonia : IFonia
    {
        private string układ;

        public string MyUkład
        {
            get { return układ; }
            set { układ = value; }
        }
        
        public void getSound()
        {
            Console.WriteLine("Pobieram dźwięk z układu dźwiękowego o nazwie");
        }

        public void volumeUp(double s)
        {
            Debug.WriteLine("Podgłośniono o {0}", s);
        }

        public void volumeDown(double s)
        {
            Debug.WriteLine("Przyciszono o {0}", s);
        }


        public void wzmacniacz(IWzmacniacz I)
        {
            Console.WriteLine(I.ToString());
        }
    }
}
