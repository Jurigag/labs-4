﻿using ComponentFramework;
using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Component2
{
    public class Układ_Fonii : AbstractComponent
    {
        private IWzmacniacz wzmacniacz = new Wzmacniacz();

        public override string ToString()
        {
            return "Wzmacniacz 1";
        }

        public static IWzmacniacz GetWzmacniacz(object component)
        {
            return (component as Układ_Fonii).wzmacniacz;
        }

        public Układ_Fonii()
        {
            RegisterProvidedInterface(typeof(IWzmacniacz), wzmacniacz);
        }

        public override void InjectInterface(Type type, object impl)
        {
            RegisterProvidedInterface(type, impl);
        }
    }
}
