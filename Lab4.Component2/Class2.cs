﻿using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Component2
{
    class Wzmacniacz : IWzmacniacz
    {
        public void wzmacniajDzwiek()
        {
            Console.WriteLine("Wzmanicam Dźwięk");
        }

        public void czyscDzwiek()
        {
            Console.WriteLine("Czyszcze Dźwięk");
        }
    }
}
