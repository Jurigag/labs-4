﻿using System;
using Mono.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using Lab4.Main;
using System.Reflection;
using System.Diagnostics;

namespace Lab4.Test
{
    [TestFixture]
    [TestClass]
    public class P3_DifferentImplementation
    {
        [Test]
        [TestMethod]
        public void P3__Component2B_Library_Should_Exist()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2B);

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P3__Contract_And_Component2_Libraries_Should_Be_Different()
        {
            // Arrange
            var contract = Assembly.GetAssembly(LabDescriptor.RequiredInterface);
            var component2 = Assembly.GetAssembly(LabDescriptor.Component2);
            var component2B = Assembly.GetAssembly(LabDescriptor.Component2B);

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component2B));
            Assert.That(component2, Is.Not.EqualTo(component2B));
        }

        [Test]
        [TestMethod]
        public void P3__Components2B_Library_Should_Not_Be_Main()
        {
            // Arrange
            var component2b = Assembly.GetAssembly(LabDescriptor.Component2B);
            var main = Assembly.GetAssembly(typeof(LabDescriptor));

            // Assert
            Assert.That(component2b, Is.Not.EqualTo(main));
        }

        [Test]
        [TestMethod]
        public void P3__Component2B_Library_Should_Not_Depend_On_Other_Components_Directly()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2B);
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").EqualTo("Lab4.Contract")
                                        .Or.Property("Name").EqualTo("ComponentFramework")
                                        .Or.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P3__GetInstanceOfRequiredInterface_Shoud_Return_Object_Implementing_RequiredInterface()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2B);

            // Act
            var impl = LabDescriptor.GetInstanceOfRequiredInterface(component);

            // Assert
            Assert.That(impl, Is.InstanceOf(LabDescriptor.RequiredInterface));
        }

        [Test]
        [TestMethod]
        public void P3__RequiredInterface_Should_Be_Accessible_After_Registering_Component2B()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2B);

            // Act
            LabDescriptor.RegisterComponent(container, component2);
            var impl = LabDescriptor.ResolveInterface(container, LabDescriptor.RequiredInterface);

            // Assert
            Assert.That(impl, Is.Not.Null);
            Assert.That(impl, Is.InstanceOf(LabDescriptor.RequiredInterface));
        }

        [Test]
        [TestMethod]
        public void P3__RequiredInterface_From_Container_Should_Be_The_Same_Instance_As_From_GetInstanceOfRequiredInterface()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component2 = Activator.CreateInstance(LabDescriptor.Component2B);

            // Act
            LabDescriptor.RegisterComponent(container, component2);
            var impl1 = LabDescriptor.ResolveInterface(container, LabDescriptor.RequiredInterface);
            var impl2 = LabDescriptor.GetInstanceOfRequiredInterface(component2);

            // Assert
            Assert.That(impl1, Is.SameAs(impl2));
        }

        [Test]
        [TestMethod]
        public void P3__Component2B_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2B);
            var types = assembly.GetExportedTypes();

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            Assert.That(types, Has.All.EqualTo(LabDescriptor.Component2B));
        }

        [Test]
        [TestMethod]
        public void P3__Component2B_Should_Be_Instantiable()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2B);

            // Assert
            Assert.That(component, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P3__Dependencies_Should_Be_Resolved_After_Registering_Component1_And_Component2B()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);
            var component2B = Activator.CreateInstance(LabDescriptor.Component2B);

            // Act
            LabDescriptor.RegisterComponent(container, component1);
            LabDescriptor.RegisterComponent(container, component2B);

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }

        [Test]
        [TestMethod]
        public void P3__Component2_And_Component2B_Should_Have_Different_Code()
        {
            // Arrange
            var componentA = Activator.CreateInstance(LabDescriptor.Component2);
            var implA = LabDescriptor.GetInstanceOfRequiredInterface(componentA);
            var componentB = Activator.CreateInstance(LabDescriptor.Component2B);
            var implB = LabDescriptor.GetInstanceOfRequiredInterface(componentB);
            
            // Act
            foreach (var method in LabDescriptor.RequiredInterface.GetMethods())
            {
                var ilA = implA.GetType().GetMethod(method.Name).GetMethodBody().GetILAsByteArray();
                var ilB = implB.GetType().GetMethod(method.Name).GetMethodBody().GetILAsByteArray();
                
                // Assert
                Assert.That(ilA, Is.Not.EquivalentTo(ilB));
            }
        }

        [Test]
        [TestMethod]
        public void P3__EntryPoint_In_Main_And_MainB_Should_Be_The_Same()
        {
            // Arrange
            var assemblyA = Assembly.UnsafeLoadFrom("Lab4.Main.exe");
            var assemblyB = Assembly.UnsafeLoadFrom(@"..\..\..\Lab4.MainB\bin\Debug\Lab4.MainB.exe");

            var methodA = assemblyA.EntryPoint;
            var enA = methodA.GetInstructions().GetEnumerator();

            var methodB = assemblyB.EntryPoint;
            var enB = methodB.GetInstructions().GetEnumerator();

            // Assert
            while (enA.MoveNext())
            {
                var next = enB.MoveNext();
                Assert.That(next);
                Assert.That(enA.Current.OpCode, Is.EqualTo(enB.Current.OpCode));
                Assert.That(enA.Current.Offset, Is.EqualTo(enB.Current.Offset));
                Assert.That(enA.Current.Size, Is.EqualTo(enB.Current.Size));

                if (enA.Current.Operand != null)
                {
                    Assert.That(enA.Current.Operand.GetType(), Is.EqualTo(enB.Current.Operand.GetType()));

                    var miA = (enA.Current.Operand as MethodInfo);
                    if (miA != null)
                    {
                        var miB = (enB.Current.Operand as MethodInfo);
                        Assert.That(miA.Name, Is.EqualTo(miB.Name));
                    }
                    else
                    {
                        var fiA = (enA.Current.Operand as FieldInfo);
                        if (fiA != null)
                        {
                            var fiB = (enB.Current.Operand as FieldInfo);
                            Assert.That(fiA.Name, Is.EqualTo(fiB.Name));
                            Assert.That(fiA.FieldType, Is.EqualTo(fiB.FieldType));
                        }
                        else
                        {
                            Assert.That(enA.Current.Operand, Is.EqualTo(enB.Current.Operand));
                        }
                    }
                }
            }
        }

        [Test]
        [TestMethod]
        public void P3__Main_And_MainB_Should_Have_Different_Output()
        {
            // Arrange
            var pA = new Process();
            pA.StartInfo.FileName = "Lab4.Main.exe";
            pA.StartInfo.UseShellExecute = false;
            pA.StartInfo.RedirectStandardOutput = true;

            var pB = new Process();
            pB.StartInfo.FileName = @"..\..\..\Lab4.MainB\bin\Debug\Lab4.MainB.exe";
            pB.StartInfo.UseShellExecute = false;
            pB.StartInfo.RedirectStandardOutput = true;
            
            // Act
            pA.Start();
            var outputA = pA.StandardOutput.ReadToEnd();
            pA.WaitForExit();

            pB.Start();
            var outputB = pB.StandardOutput.ReadToEnd();
            pB.WaitForExit();
            
            // Assert
            Assert.That(outputA, Is.Not.EqualTo(outputB));
        }
    }
}


